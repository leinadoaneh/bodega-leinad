
from django.conf.urls import url, include
from django.contrib import admin
from django.conf import settings
from django.conf.urls.static import static
urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^usuarios/', include('apps.usuarios.urls', namespace='usuarios')),
    url(r'^recursos/', include('apps.recursos.urls', namespace='recursos')),
    url(r'^personal/', include('apps.personal.urls', namespace='personal')),
    url(r'^', include('django.contrib.auth.urls')),
    url(r'^', include('apps.home.urls', namespace='home')),
    url(r'^select2/', include('django_select2.urls')),
]+ static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)

