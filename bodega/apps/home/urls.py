from django.conf.urls import url

from django.views.generic import TemplateView
from .views import InicioView

urlpatterns = [
    url(r'^$', InicioView.as_view(), name="inicio"),
    url(r'^home$', TemplateView.as_view(template_name='home.html'), name="show_home"),
]