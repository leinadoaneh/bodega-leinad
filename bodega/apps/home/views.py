from django.views.generic import TemplateView
from django.contrib.auth.models import Group, Permission

from apps.usuarios.models import User

class InicioView(TemplateView):
    template_name = "home.html"

    def get_context_data(self, **kwargs):
        context = super(InicioView, self).get_context_data(**kwargs)
        total_usuarios = User.objects.all().count()
        if total_usuarios == 0:
            password = "prueba"
            user = User.objects.create_user('admin', 'root@gmail.com', password)
            user.set_password(password)
            user.username = "admin"
            user.first_name = 'Administrador'
            user.is_superuser = True
            user.is_staff = True
            user.cargo = 'Administrador'
            user.save()

        context['titulo']='Aplicación para gestionar usuarios '
        return context

