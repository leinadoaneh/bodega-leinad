from django.contrib import messages
from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse_lazy,reverse
from django.views.generic import FormView
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.detail import DetailView
from django.contrib.auth.mixins import LoginRequiredMixin

from django.shortcuts import redirect
from django.http import JsonResponse
from .models import *
from .forms import *


class RegistrarCategoria(LoginRequiredMixin,SuccessMessageMixin,CreateView):
    model = CategoriaRecurso
    form_class = CategoriaForm
    template_name = 'categoria.html'
    success_url = reverse_lazy('recursos:registrar_categoria')
    success_message = "Se ha registrado exitosamente la categoría "

    def get_context_data(self, **kwargs):
        context = super(RegistrarCategoria,self).get_context_data()
        context['titulo'] = 'Registrar categorias para recursos'
        context['lista_categorias'] = CategoriaRecurso.objects.all()
        return context


class EditarCategoria(LoginRequiredMixin,SuccessMessageMixin,UpdateView):
    model = CategoriaRecurso
    form_class = CategoriaForm
    template_name = 'categoria.html'
    success_url = reverse_lazy('recursos:registrar_categoria')
    success_message = "Se ha editado exitosamente la categoría "

    def get_context_data(self, **kwargs):
        context = super(EditarCategoria,self).get_context_data()
        context['titulo'] = 'Editar categoría para recursos'
        context['lista_categorias'] = CategoriaRecurso.objects.all()
        return context


class RegistrarMarca(LoginRequiredMixin,SuccessMessageMixin,CreateView):
    model = MarcaRecurso
    form_class = MarcaForm
    template_name = 'marcas.html'
    success_url = reverse_lazy('recursos:registrar_marca')
    success_message = "Se ha registrado exitosamente la marca "

    def get_context_data(self, **kwargs):
        context = super(RegistrarMarca,self).get_context_data()
        context['titulo'] = 'Registrar marcas para recursos'
        context['lista_marcas'] = MarcaRecurso.objects.all()
        return context


class EditarMarca(LoginRequiredMixin,SuccessMessageMixin,UpdateView):
    model = MarcaRecurso
    form_class = MarcaForm
    template_name = 'marcas.html'
    success_url = reverse_lazy('recursos:registrar_marca')
    success_message = "Se ha editado exitosamente la marca "

    def get_context_data(self, **kwargs):
        context = super(EditarMarca,self).get_context_data()
        context['titulo'] = 'Editar marca para recursos'
        context['lista_marcas'] = MarcaRecurso.objects.all()
        return context


class RegistrarRecurso(LoginRequiredMixin,SuccessMessageMixin,CreateView):
    model = Recurso
    form_class = RecursoForm
    template_name = 'recursos.html'
    success_url = reverse_lazy('recursos:registrar_recurso')
    success_message = "Se ha registrado exitosamente el recurso"

    def get_context_data(self, **kwargs):
        context = super(RegistrarRecurso,self).get_context_data()
        context['titulo'] = 'Registrar recursos'
        context['lista_recursos'] = Recurso.objects.all()
        return context


class EditarRecurso(LoginRequiredMixin,SuccessMessageMixin,UpdateView):
    model = Recurso
    form_class = RecursoForm
    template_name = 'recursos.html'
    success_url = reverse_lazy('recursos:registrar_recurso')
    success_message = "Se ha editado exitosamente el recurso "

    def get_context_data(self, **kwargs):
        context = super(EditarRecurso,self).get_context_data()
        context['titulo'] = 'Editar recurso'
        context['lista_recursos'] = Recurso.objects.all()
        return context


class DetalleRecurso(LoginRequiredMixin,SuccessMessageMixin,DetailView):
    model = Recurso
    template_name = 'detalle_recurso.html'
    context_object_name = 'recurso'

    def get_context_data(self, **kwargs):
        context = super(DetalleRecurso,self).get_context_data()
        context['titulo'] = 'Detalles de recurso'
        context['lista_recursos'] = Recurso.objects.all()
        return context


class AsignarRecursoVarios(LoginRequiredMixin,SuccessMessageMixin,FormView):
    form_class = AsignacionVariosRecursosForm
    template_name = 'asignaciones.html'
    success_url = reverse_lazy('recursos:asignar_varios_recursos')
    success_message = "Se ha registrado exitosamente los recursos"

    def get_form_kwargs(self):
        kwargs = super(AsignarRecursoVarios, self).get_form_kwargs()
        kwargs['request'] = self.request
        no_asignados = []
        try:

            asignaciones=AsignacionRecurso.objects.filter(esta_asignado=True).values_list('recurso_id')
            recursos= Recurso.objects.exclude(id__in=asignaciones).values_list('id')
            no_asignados = recursos
        except Exception:
            pass
        kwargs['no_asignados'] = no_asignados
        return kwargs

    def form_valid(self, form):
        recursos_id = form.cleaned_data['recurso']
        persona = form.cleaned_data['persona']
        print(recursos_id,persona)
        for recurso in Recurso.objects.filter(id__in=recursos_id):
            nueva_asignacion =  AsignacionRecurso(recurso=recurso,persona=persona)
            nueva_asignacion.save()

        return super(AsignarRecursoVarios, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(AsignarRecursoVarios,self).get_context_data()
        context['titulo'] = 'Asignar recursos'
        context['lista_recursos'] = Recurso.objects.all()
        return context



def obtener_recursos(request):
    responsable = request.GET.get('responsable')
    recursos = Recurso.objects.none()
    options = ''
    if responsable:
        asignaciones= AsignacionRecurso.objects.filter(persona=responsable,esta_asignado=True).values_list('recurso_id')
        recursos = Recurso.objects.filter(id__in=asignaciones)
        print(asignaciones,recursos)
    for recurso in recursos:
        options += '<option value="%s">%s</option>' % (
            recurso.id, '(%s) %s' % (recurso.codigo, recurso.nombre)
        )
    response = {}
    response['recursos'] = options
    return JsonResponse(response)


class TrasladarRecursos(LoginRequiredMixin,SuccessMessageMixin,FormView):
    form_class = TrasladoRecursoForm
    template_name = 'trasladar_recursos.html'
    success_url = reverse_lazy('recursos:asignar_varios_recursos')
    success_message = "Se han trasladado exitosamente los recursos"

    def get_form_kwargs(self):
        kwargs = super(TrasladarRecursos, self).get_form_kwargs()
        kwargs['request'] = self.request
        no_asignados = []
        try:

            asignaciones=AsignacionRecurso.objects.filter(esta_asignado=True).values_list('recurso_id')
            recursos= Recurso.objects.exclude(id__in=asignaciones).values_list('id')
            no_asignados = recursos
        except Exception:
            pass
        kwargs['no_asignados'] = no_asignados
        return kwargs

    def form_valid(self, form):
        recursos_id = form.cleaned_data['recurso']
        responsable = form.cleaned_data['responsable']
        persona = form.cleaned_data['persona']
        for recurso in Recurso.objects.filter(id__in=recursos_id):
            asignacion_antigua=AsignacionRecurso.objects.get(recurso=recurso,persona=responsable)
            asignacion_antigua.esta_asignado=False
            asignacion_antigua.save()
            nueva_asignacion = AsignacionRecurso(recurso=recurso,persona=persona)
            nueva_asignacion.save()
        print(recursos_id,persona)

        return super(TrasladarRecursos, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(TrasladarRecursos,self).get_context_data()
        context['titulo'] = 'Trasladar recursos'
        context['lista_recursos'] = Recurso.objects.all()
        return context

@login_required
def retirar_recurso(request,pk):
    persona_id = ''
    try:
        asignacion = AsignacionRecurso.objects.get(recurso__id=pk,esta_asignado=True)
        persona_id = asignacion.persona_id
        asignacion.esta_asignado=False
        asignacion.save()

    except Exception as e:
        print('exception: ', e)
    messages.success(request, "Se ha retirado exitosamente el recurso")
    return redirect(reverse('personal:ver_recursos_personal', kwargs={'pk': persona_id}))


