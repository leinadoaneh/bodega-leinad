from django.db import models

# Create your models here.


class CategoriaRecurso(models.Model):

    nombre = models.CharField(max_length=100)

    def __str__(self):
        return self.nombre


class MarcaRecurso(models.Model):

    nombre = models.CharField(max_length=100)
    descripcion = models.TextField(verbose_name='Descripción')

    def __str__(self):
        return self.nombre


class Recurso(models.Model):

    nombre = models.CharField(max_length=100)
    descripcion = models.TextField(verbose_name='Descripción')
    codigo = models.CharField(max_length=30,verbose_name='Código',unique=True)
    marca = models.ForeignKey(MarcaRecurso,related_name='marca_recursos')
    categoria = models.ForeignKey(CategoriaRecurso, related_name='categoria_recursos',verbose_name='Categoría')

    def __str__(self):
        return '(%s) %s' % (self.codigo, self.nombre)


class AsignacionRecurso(models.Model):
    recurso = models.ForeignKey(Recurso,related_name='asignaciones')
    persona = models.ForeignKey('personal.Personal')
    esta_asignado = models.BooleanField(default=True)
    fecha = models.DateTimeField(auto_now_add=True)

    class Meta:
        ordering = ['-fecha']
