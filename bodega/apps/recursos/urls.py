from django.conf.urls import include, url
from apps.recursos import views,forms


urlpatterns = [
    url(r'^registrar-categoria$', views.RegistrarCategoria.as_view(), name="registrar_categoria"),
    url(r'^editar-categoria/(?P<pk>\d+)$', views.EditarCategoria.as_view(), name="editar_categoria"),
    url(r'^registrar-marcas', views.RegistrarMarca.as_view(), name="registrar_marca"),
    url(r'^editar-marcas/(?P<pk>\d+)$', views.EditarMarca.as_view(), name="editar_marca"),
    url(r'^registrar-recurso', views.RegistrarRecurso.as_view(), name="registrar_recurso"),
    url(r'^editar-recurso/(?P<pk>\d+)$', views.EditarRecurso.as_view(), name="editar_recurso"),
    url(r'^detalle-recurso/(?P<pk>\d+)$', views.DetalleRecurso.as_view(), name="detalle_recurso"),
    url(r'^retirar-recurso/(?P<pk>\d+)$', views.retirar_recurso, name="retirar_recurso"),
    url(r'^asignar-varios', views.AsignarRecursoVarios.as_view(), name="asignar_varios_recursos"),
    url(r'^trasladar', views.TrasladarRecursos.as_view(), name="trasladar_recursos"),
    url(r'^ajax/obtener_recursos/$', views.obtener_recursos, name='obtener_recursos'),
]