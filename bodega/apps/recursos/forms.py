from django import forms
from django_select2.forms import ModelSelect2MultipleWidget, Select2Widget, ModelSelect2Widget, Select2MultipleWidget
from .models import *
from apps.personal.models import Personal


class CategoriaForm(forms.ModelForm):
    class Meta:
        model = CategoriaRecurso
        fields= ('nombre',)


class MarcaForm(forms.ModelForm):
    class Meta:
        model = MarcaRecurso
        fields= ('nombre','descripcion')


class RecursoForm(forms.ModelForm):
    class Meta:
        model = Recurso
        fields = ('nombre','descripcion','codigo','marca','categoria')
        widgets= {
            'marca':Select2Widget,
            'categoria':Select2Widget,
        }


class AsignacionVariosRecursosForm(forms.Form):
    persona=forms.ModelChoiceField(queryset=Personal.objects.all(),widget=Select2Widget)
    recurso = forms.MultipleChoiceField(widget=Select2MultipleWidget)

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        no_asignados = kwargs.pop('no_asignados')
        super(AsignacionVariosRecursosForm, self).__init__(*args, **kwargs)
        choices = []
        for item in Recurso.objects.filter(id__in=no_asignados):
            recurso_actual = (
                item.id, '(%s) %s' % (item.codigo, item.nombre)
            )
            choices.append(recurso_actual)
        self.fields['recurso'].choices= choices


class TrasladoRecursoForm(forms.Form):
    responsable = forms.ModelChoiceField(queryset=Personal.objects.all(), widget=Select2Widget)
    persona=forms.ModelChoiceField(queryset=Personal.objects.all(),widget=Select2Widget)
    recurso = forms.MultipleChoiceField(widget=Select2MultipleWidget)

    def __init__(self, *args, **kwargs):
        self.request = kwargs.pop('request')
        no_asignados = kwargs.pop('no_asignados')
        super(TrasladoRecursoForm, self).__init__(*args, **kwargs)
        choices = []
        for item in Recurso.objects.filter(asignaciones__esta_asignado=True):
            recurso_actual = (
                item.id, '(%s) %s' % (item.codigo, item.nombre)
            )
            choices.append(recurso_actual)
        self.fields['recurso'].choices= choices

    def clean(self):
        form_data = self.cleaned_data

        if form_data["responsable"] == form_data["persona"]:
            self._errors["responsable"] = ["El responsable no puede ser igual a la persona"]

        if len(form_data["recurso"]) == 0:
            self._errors["recurso"] = ["Debe seleccionar al menos un recurso"]

        return form_data



