from django.db import models
from bodega import variables_choices
# Create your models here.


class Personal(models.Model):
    nombre = models.CharField(max_length=100)
    apellidos = models.CharField(max_length=100)
    identificacion = models.CharField(max_length=30,unique=True)
    genero = models.CharField(max_length=50, choices=variables_choices.GENEROS, verbose_name="Género")
    telefono_fijo = models.CharField(max_length=15, blank=True)
    telefono_celular = models.CharField(max_length=15, blank=True)
    direccion = models.CharField(max_length=100,blank=True)
    email= models.EmailField()

    def __str__(self):
        return '%s %s' % (self.nombre, self.apellidos)

    def obtener_recursos_asignados(self):
        from apps.recursos.models import  Recurso
        return Recurso.objects.filter(asignaciones__esta_asignado=True,asignaciones__persona=self)