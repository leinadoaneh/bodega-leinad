from django.contrib.messages.views import SuccessMessageMixin
from django.contrib.auth.decorators import login_required
from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView, TemplateView, FormView,View
from django.views.generic.edit import CreateView, UpdateView
from django.views.generic.detail import DetailView
from django.contrib.auth.mixins import LoginRequiredMixin

from django.shortcuts import render, redirect, get_object_or_404
from .models import *
from .forms import *
# Create your views here.


class RegistrarPersonal(LoginRequiredMixin,SuccessMessageMixin,CreateView):
    model = Personal
    form_class = PersonalForm
    template_name = 'personal.html'
    success_url = reverse_lazy('personal:registrar_personal')
    success_message = "Se ha registrado exitosamente el personal "

    def get_context_data(self, **kwargs):
        context = super(RegistrarPersonal,self).get_context_data()
        context['titulo'] = 'Registrar personal'
        return context


class EditarPersonal(LoginRequiredMixin,SuccessMessageMixin,UpdateView):
    model = Personal
    form_class = PersonalForm
    template_name = 'personal.html'
    success_url = reverse_lazy('personal:lista_personal')
    success_message = "Se ha editado exitosamente el personal "

    def get_context_data(self, **kwargs):
        context = super(EditarPersonal,self).get_context_data()
        context['titulo'] = 'Editar personal'
        return context



class ListarPersonal(LoginRequiredMixin,ListView):
    model = Personal
    template_name = 'lista_personal.html'
    context_object_name = "lista_personal"


class VerRecursos(LoginRequiredMixin,DetailView):
    model = Personal
    template_name = 'lista_recursos_personal.html'
    context_object_name = "personal"
