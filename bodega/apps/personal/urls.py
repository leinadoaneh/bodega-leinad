from django.conf.urls import include, url
from apps.personal import views,forms


urlpatterns = [
    url(r'^registrar$', views.RegistrarPersonal.as_view(), name="registrar_personal"),
    url(r'^editar/(?P<pk>\d+)$', views.EditarPersonal.as_view(), name="editar_personal"),
    url(r'^listar$', views.ListarPersonal.as_view(), name="lista_personal"),
    url(r'^ver-recursos/(?P<pk>\d+)$', views.VerRecursos.as_view(), name="ver_recursos_personal"),
]