from django.db import models
from django.contrib.auth.models import AbstractUser
from bodega import variables_choices
# Create your models here.


class User(AbstractUser):

    identificacion = models.CharField(max_length=30)
    telefono_fijo = models.CharField(max_length=15,blank=True)
    telefono_celular = models.CharField(max_length=15,blank=True)
    foto = models.ImageField(upload_to='media', blank=True, null=True, )
    direccion = models.CharField(max_length=100)
    cargo = models.CharField(max_length=50, choices=variables_choices.CARGOS, verbose_name='Perfil')
    genero = models.CharField(max_length=50, choices=variables_choices.GENEROS, verbose_name="Género")


    def __str__(self):
        return '%s %s' % (self.last_name, self.first_name)

    # Función que devuelve la pagina html del dashboard dependiendo del tipo de usuario
    def obtener_pagina_dashboard(self):
        if self.cargo == 'Administrador':
            return "inicio_administrador.html"

        return "inicio_administrador.html"

#Funcion que devuelve los datos para el dashboard dependiendo del tipo de usuario
    def datos_dashboard(self):
        from apps.personal.models import Personal
        from apps.recursos.models import Recurso
        datos = {}
        datos['lista_personal']= Personal.objects.all()
        recursos_asignados = Recurso.objects.filter(asignaciones__esta_asignado=True).values_list('id')
        datos['lista_recursos'] = Recurso.objects.exclude(id__in=recursos_asignados) #recursos sin asignar
        return datos