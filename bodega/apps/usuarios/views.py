from django.contrib import messages
from django.core.urlresolvers import reverse_lazy
from django.views.generic import ListView, TemplateView, FormView,View
from django.views.generic.edit import CreateView, UpdateView

from django.contrib.auth.mixins import LoginRequiredMixin

from django.contrib.auth import authenticate, login, logout
from django.shortcuts import render, redirect, get_object_or_404
from .models import *
from .forms import *

# Create your views here.


class RegistrarUsuario(LoginRequiredMixin,CreateView):
    model = User
    form_class = UserForm
    template_name = 'usuario.html'
    success_url = reverse_lazy('usuarios:lista_usuarios')

    def get_context_data(self, **kwargs):
        context = super(RegistrarUsuario,self).get_context_data()
        context['titulo'] = 'Registrar usuario'
        return context

    def form_valid(self, form):
        self.object=form.save()
        messages.add_message(self.request, messages.SUCCESS, "Se ha registrado exitosamente al usuario ")
        return super(RegistrarUsuario, self).form_valid(form)


class EditarUsuario(LoginRequiredMixin,UpdateView):
    model = User
    form_class = EditUserForm
    template_name = 'usuario.html'
    success_url = reverse_lazy('usuarios:lista_usuarios')
    context_object_name = 'usuario'

    def form_valid(self, form):
        self.object=form.save()
        messages.add_message(self.request, messages.SUCCESS, "Se ha editado exitosamente al usuario ")
        return super(EditarUsuario, self).form_valid(form)

    def get_context_data(self, **kwargs):
        context = super(EditarUsuario,self).get_context_data()
        context['titulo'] = 'Editar usuario'
        return context


class ListarUsuarios(LoginRequiredMixin,ListView):
    model = User
    template_name = 'lista_usuarios.html'
    context_object_name = "lista_usuarios"


class Login(FormView):
    form_class = LoginForm
    template_name = 'login.html'

    def get(self, request, *args, **kwargs):
        if self.request.user.is_authenticated:
            return redirect('usuarios:dashboard')
        return super(Login,self).get(request)

    def form_valid(self, form):
        mensaje = ""
        user = authenticate(username=form.cleaned_data['username'], password=form.cleaned_data['password'])
        if user is not None:
            if user.is_active:
                self.success_url = reverse_lazy('usuarios:dashboard')
                login(self.request, user)
                return super(Login, self).form_valid(form)
            else:
                mensaje = ("El usuario %s no esta activo") % (user.username)
        else:
            mensaje = "El usuario no existe o la contraseña es incorrecta"
        form.add_error('username', mensaje)
        return super(Login, self).form_invalid(form)

    def form_invalid(self, form):
        return super(Login, self).form_invalid(form)


def Logout(request):
    logout(request)
    return redirect(reverse_lazy('usuarios:login'))


class InicioUsuario(LoginRequiredMixin, TemplateView):

    def get_context_data(self, **kwargs):
        context = super(InicioUsuario, self).get_context_data(**kwargs)
        self.template_name = self.request.user.obtener_pagina_dashboard()
        context.update(self.request.user.datos_dashboard())
        return context

