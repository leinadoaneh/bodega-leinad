from django import forms
from django.contrib.auth.forms import UserCreationForm,UserChangeForm,PasswordChangeForm
from django_select2.forms import ModelSelect2MultipleWidget, Select2Widget, ModelSelect2Widget, Select2MultipleWidget
from .models import *





class UserForm(UserCreationForm):
    class Meta:
        model = User
        fields= ('username','email','identificacion','first_name','last_name','cargo','genero')
        widgets = {
            'cargo':Select2Widget,
            'genero': Select2Widget,
        }

    def __init__(self, *args, **kwargs):
        super(UserForm, self).__init__(*args, **kwargs)


class EditUserForm(forms.ModelForm):
    class Meta:
        model = User
        fields= ('first_name','last_name','telefono_fijo','telefono_celular','email','foto','cargo','genero')
        widgets = {
            'cargo': Select2Widget,
            'genero': Select2Widget,
        }

    def __init__(self, *args, **kwargs):
        super(EditUserForm, self).__init__(*args, **kwargs)


class LoginForm(forms.Form):


    username = forms.CharField(max_length = 50,
            widget=forms.TextInput(attrs ={
                    'id':'usernameInput',
                    'placeholder': 'Nombre de usuario',
                    'class':'form-control'
                }))
    password = forms.CharField(max_length = 50,
            widget = forms.TextInput(attrs = {
                    'type' : 'password',
                    'id':'passwordInput',
                    'placeholder': 'Contraseña',
                    'class':'form-control'
                }))