from django.conf.urls import  url
from apps.usuarios import views
from django.contrib.auth import views as auth_views



urlpatterns = [
    url(r'^password_change$', auth_views.password_change,{'post_change_redirect': 'usuarios:password_change_done' }, name="password_change"),
    url(r'^password_change_done$', auth_views.password_change_done, name='password_change_done'),

    url(r'^registrar$', views.RegistrarUsuario.as_view(), name="registrar_usuario"),
    url(r'^editar/(?P<pk>\d+)$', views.EditarUsuario.as_view(), name="editar_usuario"),
    url(r'^listar$', views.ListarUsuarios.as_view(), name="lista_usuarios"),
    url(r'^dashboard$', views.InicioUsuario.as_view(), name="dashboard"),

    url(r'^login$', views.Login.as_view(), name="login"),
    url(r'^logout$', views.Logout, name="logout"),


]